﻿using CoolParking.BL.DtoModels;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Text.RegularExpressions;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        Regex regex = new Regex(@"[A-Z]{2}-\d{4}-[A-Z]{2}");
        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        [HttpGet]
        [Route("api/transactions/last")]
        public IActionResult GetTransactions()
        {
            var transaction = _parkingService.GetLastParkingTransactions();
            return Ok(transaction);
        }

        [HttpGet]
        [Route("api/transactions/all")]
        public IActionResult GetTransactionsFromLog(string id)
        {
            string transaction = String.Empty;
            try
            {
                transaction = _parkingService.ReadFromLog();
            }
            catch (InvalidOperationException)
            {
                return NotFound();

            }

            return Ok(transaction);
        }

        [HttpPut]
        [Route("api/transactions/topUpVehicle")]
        public IActionResult TopUpVehicle([FromBody] TopUpvehicleDto topUpvehicleDto)
        {
            if (!regex.IsMatch(topUpvehicleDto.Id))
            {
                return BadRequest();
            }
            try
            {
                _parkingService.TopUpVehicle(topUpvehicleDto.Id, topUpvehicleDto.Sum);
            }
            catch (ArgumentException)
            {
                return NotFound();
            }

            var vehicle = Vehicle.GetVehicleById(topUpvehicleDto.Id);
            return Ok(vehicle);
        }

    }
}
