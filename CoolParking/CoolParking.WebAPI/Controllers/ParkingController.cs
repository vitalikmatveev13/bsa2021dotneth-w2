﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        Regex regex = new Regex(@"[A-Z]{2}-\d{4}-[A-Z]{2}");
        public ParkingController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet]
        [Route("api/parking/balance")]
        public IActionResult GetBalance()
        {
            var balance = _parkingService.GetBalance();
            return Ok(balance);
        }

        [HttpGet]
        [Route("api/parking/capacity")]
        public IActionResult GetCapacity()
        {
            var capacity = _parkingService.GetCapacity();
            return Ok(capacity);
        }

        [HttpGet]
        [Route("api/parking/freePlaces")]
        public IActionResult GetFreePlaces()
        {
            var freePlaces = _parkingService.GetFreePlaces();
            return Ok(freePlaces);
        }
    }
}
