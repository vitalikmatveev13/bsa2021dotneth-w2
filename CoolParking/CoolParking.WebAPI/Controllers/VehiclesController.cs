﻿using CoolParking.BL.DtoModels;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Text.RegularExpressions;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        Regex regex = new Regex(@"[A-Z]{2}-\d{4}-[A-Z]{2}");
        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet]
        [Route("api/vehicles")]
        public IActionResult GetVehicles()
        {
            var vehicles = _parkingService.GetVehicles();
            return Ok(vehicles);
        }

        [HttpGet]
        [Route("api/vehicles/{id}")]
        public IActionResult GetVehicle(string id)
        {

            if (!regex.IsMatch(id))
            {
                return BadRequest();
            }
            var vehicle = Vehicle.GetVehicleById(id);
            if (vehicle == null)
            {
                return NotFound();
            }

            return Ok(vehicle);
        }

        [HttpPost]
        [Route("api/vehicles")]
        public IActionResult AddVehicle([FromBody] VehicleDto vehicle)
        {
            if (vehicle == null || string.IsNullOrEmpty(vehicle.Id) || vehicle.Balance == 0 || vehicle.Type == 0)
            {
                return BadRequest();
            }
            if (!regex.IsMatch(vehicle?.Id))
            {
                return BadRequest();
            }
            try
            {
                Vehicle vehicle1 = new Vehicle(vehicle.Id, vehicle.Type, vehicle.Balance);
                _parkingService.AddVehicle(vehicle1);
            }
            catch (Exception e)
            {

                return BadRequest(e.Message);
            }

            return Created("api/vehicles", vehicle);
        }

        [HttpDelete]
        [Route("api/vehicles/{id}")]
        public IActionResult DeleteVehicle(string id)
        {
            if (!regex.IsMatch(id))
            {
                return BadRequest();
            }
            try
            {
                _parkingService.RemoveVehicle(id);
            }
            catch (ArgumentException)
            {
                return NotFound();
            }

            return Ok();
        }
    }
}
