﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    internal class WorkFlow
    {
        private IParkingService _parkingService;
        public WorkFlow(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        public bool Run()
        {
            DisplayMainMessage();
            Actions();
            Console.WriteLine("Do you want continue? Y/N");
            string answer = Console.ReadLine().ToLower();
            return answer == "y" ? true : false;
        }
        void Actions()
        {
            string number = Console.ReadLine();
            switch (number)
            {
                case "1":
                    Console.WriteLine($"Current balance is {_parkingService.GetBalance()}");
                    break;
                case "2":
                    Console.WriteLine($"The capacity of the parking is {_parkingService.GetCapacity()}");
                    break;
                case "3":
                    Console.WriteLine($"Last transactions is {_parkingService.GetLastParkingTransactions()}");
                    break;
                case "4":
                    Console.WriteLine($"Transport list: {_parkingService.GetVehicles()}");
                    break;
                case "5":
                    Console.WriteLine("Enter transport id, type" +
                            "(1 - Bus, 2 - PassengerCar, 3 - Truck and 4 - Motorcycle) and how much money you give");
                    string id = Console.ReadLine();
                    VehicleType type = (VehicleType)Convert.ToInt32(Console.ReadLine());
                    decimal money = Convert.ToDecimal(Console.ReadLine());
                    Vehicle vehicle = new Vehicle(id, type, money);
                    _parkingService.AddVehicle(vehicle);
                    Console.WriteLine("Added\n");
                    break;
                case "6":
                    Console.WriteLine("Enter the id of transport you want to remove:");
                    string idRemove = Console.ReadLine();
                    _parkingService.RemoveVehicle(idRemove);
                    Console.WriteLine("Deleted\n");
                    break;
                case "7":
                    Console.WriteLine("Enter sum and id of transport:");
                    decimal sum = Convert.ToDecimal(Console.ReadLine());
                    string ids = Console.ReadLine();
                    _parkingService.TopUpVehicle(ids, sum);
                    Console.WriteLine($"Now {ids} is staying on parking with balance {sum}\n");
                    break;
                case "8":
                    Console.WriteLine(_parkingService.ReadFromLog());
                    break;
                case "9":
                    Console.Clear();
                    break;
                case "10":
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Entered incorrect number, try again");
                    break;
            }
        }
        void DisplayMainMessage()
        {
            Console.WriteLine("Enter a number what have to do:\n" +
               "1:Display current balance of parking\n" +
               "2:Display capacity of the parking\n" +
               "3:Display history of transactions for current period\n" +
               "4:Display list of transports which on the parking now\n" +
               "5:Add transpory on parking\n" +
               "6:Remove transport from the parking\n" +
               "7:Top up balance of a specific transport\n" +
               "8:Read information from log file(can be a lot of text)\n" +
               "9:Clear console and come back)\n" +
               "10:Exit program\n");
        }
    }
}
