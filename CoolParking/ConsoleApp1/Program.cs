﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;

namespace ConsoleApp1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ParkingService parkingService = new ParkingService(new TimerService(), new TimerService(), new LogService(Settings.LogPath));
            WorkFlow workFlow = new WorkFlow(parkingService);
            ApiWorkFlow apiWorkFlow = new ApiWorkFlow();
            var status = true;
            while (status)
            {
                status = apiWorkFlow.Run();
            }
        }
    }
}
