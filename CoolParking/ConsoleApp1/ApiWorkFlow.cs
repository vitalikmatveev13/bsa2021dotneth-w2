﻿using CoolParking.BL.DtoModels;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using static CoolParking.WebAPI.Controllers.ParkingController;

namespace ConsoleApp1
{
    internal class ApiWorkFlow
    {
        HttpApiRequest HttpApiRequest = new HttpApiRequest();
        public ApiWorkFlow()
        {
        }
        public bool Run()
        {
            DisplayMainMessage();
            Actions();
            Console.WriteLine("Do you want continue? Y/N");
            string answer = Console.ReadLine().ToLower();
            return answer == "y" ? true : false;
        }
        void Actions()
        {
            string number = Console.ReadLine();
            switch (number)
            {
                case "1":
                    Console.WriteLine($"Current balance is {HttpApiRequest.GetParkingHttp("balance").Result}");
                    break;
                case "2":
                    Console.WriteLine($"The capacity of the parking is {HttpApiRequest.GetParkingHttp("capacity").Result}");
                    break;
                case "3":
                    Console.WriteLine($"Last transactions is {HttpApiRequest.GetTransationHttp("last").Result}");
                    break;
                case "4":
                    Console.WriteLine($"Transport list: {HttpApiRequest.GetVehicleHttp("").Result}");
                    break;
                case "5":
                    Console.WriteLine($"Transport list: {HttpApiRequest.GetParkingHttp("freePlaces").Result}");
                    break;
                case "6":
                    Console.WriteLine("Enter transport id, type" +
                            "(1 - Bus, 2 - PassengerCar, 3 - Truck and 4 - Motorcycle) and how much money you give");
                    string id = Console.ReadLine();
                    VehicleType type = (VehicleType)Convert.ToInt32(Console.ReadLine());
                    decimal money = Convert.ToDecimal(Console.ReadLine());
                    Vehicle vehicle = new Vehicle(id, type, money);
                    var jsonVahicle = JsonSerializer.Serialize(vehicle);
                    var vehicleadded = HttpApiRequest.AddVehcilePost("", jsonVahicle).Result;
                    Console.WriteLine("Added\n");
                    break;
                case "7":
                    Console.WriteLine("Enter the id of transport you want to remove:");
                    string idRemove = Console.ReadLine();
                    var vehicleDeleted = HttpApiRequest.DeleteVehcile(idRemove).Result;
                    Console.WriteLine("Deleted\n");
                    break;
                case "8":
                    Console.WriteLine("Enter sum and id of transport:");
                    decimal sum = Convert.ToDecimal(Console.ReadLine());
                    string ids = Console.ReadLine();
                    TopUpvehicleDto _TopUpvehicleDto = new TopUpvehicleDto
                    {
                        Id = ids,
                        Sum = sum,
                    };
                    var json = JsonSerializer.Serialize(_TopUpvehicleDto);
                    var topUpVehicle = HttpApiRequest.TopUpVehicle("topUpVehicle", json).Result;
                    Console.WriteLine($"Now {ids} is staying on parking with balance {sum}\n");
                    break;
                case "9":
                    Console.WriteLine(HttpApiRequest.GetTransationHttp("all").Result); //all
                    break;
                case "10":
                    Console.Clear();
                    break;
                case "11":
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Entered incorrect number, try again");
                    break;
            }
        }
        void DisplayMainMessage()
        {
            Console.WriteLine("Enter a number what have to do:\n" +
               "1:Display current balance of parking\n" +
               "2:Display capacity of the parking\n" +
               "3:Display history of transactions for current period\n" +
               "4:Display list of transports which on the parking now\n" +
               "5:Get free places \n" +
               "6:Add transpory on parking\n" +
               "7:Remove transport from the parking\n" +
               "8:Top up balance of a specific transport\n" +
               "9:Read information from log file(can be a lot of text)\n" +
               "10:Clear console and come back)\n" +
               "11:Exit program\n");
        }
    }
}
