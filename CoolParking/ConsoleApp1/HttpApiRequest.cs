﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    internal class HttpApiRequest
    {
        static HttpClient client = new HttpClient();
        private static string _baseUrl = new string(@"https://localhost:44331/api/");
        public static async Task<string> GetParkingHttp(string LastEndpoint)
        {
            var resp = await client.GetAsync(_baseUrl + $"parking/{LastEndpoint}");
            return await resp.Content.ReadAsStringAsync();
        }
        public static async Task<string> GetTransationHttp(string LastEndpoint)
        {
            var resp = await client.GetAsync(_baseUrl + $"transactions/{LastEndpoint}");
            return await resp.Content.ReadAsStringAsync();
        }
        public static async Task<string> GetVehicleHttp(string LastEndpoint)
        {
            var resp = await client.GetAsync(_baseUrl + $"vehicles/{LastEndpoint}");
            return await resp.Content.ReadAsStringAsync();
        }
        public static async Task<string> AddVehcilePost(string LastEndpoint, string jsonData)
        {
            var resp = await client.PostAsync(_baseUrl + $"vehicles/{LastEndpoint}", new StringContent(jsonData, Encoding.UTF8, "application/json"));
            return await resp.Content.ReadAsStringAsync();
        }
        public static async Task<string> DeleteVehcile(string id)
        {
            var resp = await client.DeleteAsync(_baseUrl + $"vehicles/{id}");
            return await resp.Content.ReadAsStringAsync();
        }
        public static async Task<string> TopUpVehicle(string LastEndpoit, string json)
        {
            var resp = await client.PutAsync(_baseUrl + $"transactions/", new StringContent(json, Encoding.UTF8, "application/json"));
            return await resp.Content.ReadAsStringAsync();
        }
    }
}
