﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Collections.ObjectModel;
using System.Linq;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private Parking Parking { get; set; } = Parking.GetInstance();
        private ILogService _logService;
        private ITimerService _timerService;

        public ParkingService(ILogService logService, ITimerService timerService)
        {
            _logService = logService;
            _timerService = timerService;

        }
        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _logService = logService;
            withdrawTimer.Elapsed += (sender, e) => new TimerService().GetMoney(sender, e);
            _timerService = withdrawTimer;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (Parking.VehicleList.Count >= Settings.Capacity)
                throw new System.InvalidOperationException("Try to add auto on full parking");
            if (IsNotEqualId(vehicle.Id))
            {
                Parking.VehicleList.Add(vehicle);
                return;
            }
            throw new System.ArgumentException("Auto with this id is already stay on cool parking");
        }
        private bool IsNotEqualId(string vehicleId) => !Parking.VehicleList.Any(Vehicle => Vehicle.Id == vehicleId);
        public void Dispose()
        {
            Parking.Balance = Settings.StartBalance;
            Parking.VehicleList.Clear();
            Parking.TransactionInfos.Clear();

            _timerService?.Dispose();

            if (System.IO.File.Exists(Settings.LogPath))
                System.IO.File.Delete(Settings.LogPath);
            System.GC.Collect();
        }

        public decimal GetBalance()
        {
            return Parking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.Capacity;
        }

        public int GetFreePlaces()
        {
            return GetCapacity() - Parking.VehicleList.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return Parking.TransactionInfos.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return Parking.VehicleList.AsReadOnly();
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (!IsNotEqualId(vehicleId))
            {
                if (Vehicle.GetVehicleById(vehicleId).Balance < 0)
                {
                    throw new System.InvalidOperationException("Try to remove transport with negative balance");
                }
                Parking.VehicleList.Remove(Parking.VehicleList.Find(v => v.Id == vehicleId));
                return;
            }
            throw new System.ArgumentException("Id is not right");
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum <= 0)
                throw new System.ArgumentException("The sum less zero or equals zero");
            var vehicle = Vehicle.GetVehicleById(vehicleId);

            if (vehicle != null)
            {
                vehicle.Balance += sum;
                return;
            }
            throw new System.ArgumentException("Id is not right");
        }
    }
}
