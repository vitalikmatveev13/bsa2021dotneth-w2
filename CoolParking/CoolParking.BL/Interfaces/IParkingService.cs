﻿using System;
using System.Collections.ObjectModel;
using CoolParking.BL;
using CoolParking.BL.Models;

namespace CoolParking.BL.Interfaces
{
    public interface IParkingService : IDisposable
    {
        public decimal GetBalance();
        public int GetCapacity();
        public int GetFreePlaces();
        public ReadOnlyCollection<Vehicle> GetVehicles();
        public void AddVehicle(Vehicle vehicle);
        public void RemoveVehicle(string vehicleId);
        public void TopUpVehicle(string vehicleId, decimal sum);
        public TransactionInfo[] GetLastParkingTransactions();
        public string ReadFromLog();
    }
}
