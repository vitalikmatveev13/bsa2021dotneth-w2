﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking instance;
        public decimal Balance { get; set; }
        public List<Vehicle> VehicleList { get; set; }
        public List<TransactionInfo> TransactionInfos { get; set; }
        private Parking()
        {
            VehicleList = new List<Vehicle>();
            TransactionInfos = new List<TransactionInfo>();
        }
        public static Parking GetInstance()
        {
            if (instance == null)
            {
                instance = new Parking();
            }
            return instance;
        }
    }
}
