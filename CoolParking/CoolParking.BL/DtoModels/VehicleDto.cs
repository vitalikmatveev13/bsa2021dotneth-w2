﻿using CoolParking.BL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CoolParking.BL.DtoModels
{
    public class VehicleDto
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "balance")]
        public decimal Balance { get; set; }


        [JsonPropertyName("vehicleType")]
        public VehicleType Type { get; set; }
    }
}
