﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.BL.DtoModels
{
    public class TopUpvehicleDto
    {
        public string Id { get; set; }
        public decimal Sum { get; set; }
    }
}
